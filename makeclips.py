import os, sys, datetime
import time

entries_f = {};
entries_r = {};

def merge(entries, key, start, dur):
    if len(entries) == 0: return
    i=0;
    fp = open("files.txt", "wt");
    d = None;
    print(f"Make clip starting at: {start}, duration: {dur}");
    current_t = 0;
    for e in entries:
        time_diff = e['date'].timestamp() - current_t;
        trim = 0;
        if time_diff >= 60:
            # leave it as is
            pass;
        elif time_diff > 55:
            trim = 60.99 - time_diff;
        else:
            trim  = 5;

        current_t = e['date'].timestamp();

        args='';
        if trim > 0: args += f'-ss {trim}';

        print(f"Processing {i}: {e['filename']}, trim: {trim} ");

        os.system(f"ffmpeg -y -i {e['filename']} {args} -c copy {i}.mkv > /dev/null 2>&1");
        fp.write(f"file '{i}.mkv'\n");
        d = e['dir'];
        i+=1;
    fp.close();

    args="";
    if start != None: args += f' -ss {start}';
    if dur != None: args += f' -t {dur}';

    print(f"Writing clips/{key}_{d}.mkv");
    os.system(f"ffmpeg -y -f concat -i files.txt {args} -c copy clips/{key}_{d}.mkv > /dev/null 2>&1");

    os.unlink('files.txt');
    os.unlink('0.mkv');
    os.unlink('1.mkv');
    os.unlink('2.mkv');


    

def makeclip(filename, start, dur, delete=0):

    for f in os.listdir('.'):
        entry = {};

        if not f.endswith('.mp4'): continue;
        if len(f) != 22: continue;

        entry['filename'] = f;
        entry['year']=int(f[0:4]);
        entry['month']=int(f[4:6]);
        entry['day']=int(f[6:8]);

        entry['hour']=int(f[9:11]);
        entry['minute']=int(f[11:13]);
        entry['second']=int(f[13:15]);

        entry['type']=f[16];
        entry['dir']=f[17];

        entry['key'] = f[0:15];

        entry['date'] = datetime.datetime(entry['year'], entry['month'],entry['day'], entry['hour'], entry['minute'], entry['second']);

        if entry['dir'] == 'F':
            entries_f[entry['key']] = entry;
        else:
            entries_r[entry['key']] = entry;


    key = filename[0:15];
    print(f"key={key}");

    f_keys = sorted(entries_f.keys());
    index = f_keys.index(key);

    ef = [ entries_f[f_keys[index-2]], entries_f[f_keys[index-1]], entries_f[f_keys[index]] ];
    if key in entries_r:
        r_keys = sorted(entries_r.keys());
        index = r_keys.index(key);
        er = [ entries_r[r_keys[index-2]], entries_r[r_keys[index-1]], entries_r[r_keys[index]] ];
    else:
        er = [];

    print(f"Found F: {ef[0]['filename']}");
    print(f"       : {ef[1]['filename']}");
    print(f"       : {ef[2]['filename']}");
    if len(er) > 0:
        print(f"Found R: {er[0]['filename']}");
        print(f"       : {er[1]['filename']}");
        print(f"       : {er[2]['filename']}");

    if delete > 0:
        print("Deleting the above files in 5 seconds");
        if delete > 1:
            print(f"Also deleting clips/{key}*");
        time.sleep(5);
        for e in ef:
            print(f'Delete ' + e['filename']);
            os.unlink(e['filename']);
        for e in er:
            print(f'Delete ' + e['filename']);
            os.unlink(e['filename']);

        if delete > 1:
            print(f'Delete clips/{key}_F.mkv');
            os.unlink(f'clips/{key}_F.mkv');
            print(f'Delete clips/{key}_R.mkv');
            os.unlink(f'clips/{key}_R.mkv');
            
    else:
        merge(ef, key, start, dur);
        merge(er, key, start, dur);

start = None;
dur = None;
delete = 0;

for i in range(2,len(sys.argv)):
    if sys.argv[i] == '-d':
        delete += 1;
        continue;
    if start == None: 
        start = sys.argv[i];
        continue;
    if dur == None:
        dur = sys.argv[i];
        continue;

makeclip(sys.argv[1], start, dur, delete);




        

