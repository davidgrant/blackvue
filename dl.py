
import os, sys, datetime

IP='192.168.1.150';
os.system(f'wget "http://{IP}/blackvue_vod.cgi" -O list.txt');

files_done = []
new_files_done = [];
fp = open('.last', 'rt');
if fp:
    for line in fp:
        files_done.append(line.strip());
fp.close();

entries = [];
fp = open('list.txt');
for line in fp:
    data = line.strip().split(',');
    entry = {};
    for d in data:
        e = d.split(':');
        entry[e[0]] = e[1];

    if 'v' in entry:
        continue;

    f=entry['n'].split('/');
    entry['filename'] = f[2];


    year=int(entry['n'][8:12]);
    month=int(entry['n'][12:14]);
    day=int(entry['n'][14:16]);

    hour=int(entry['n'][17:19]);
    minute=int(entry['n'][19:21]);
    second=int(entry['n'][21:23]);

    type=entry['n'][24];
    dir=entry['n'][25];

    entry['date'] = datetime.datetime(year, month, day, hour, minute, second);
    entry['type'] = type;
    entry['dir'] = dir;
    entry['download'] = False;
    entry['key'] = entry['n'][8:23];

    # Check if the file was previously downloaded, if it was remember that it was by setting new_files_done too
    if entry['filename'] in files_done:
        new_files_done.append(entry['filename'])
        print(f"Skip {entry['filename']}, already done.");
        continue;

    entries.append(entry);
fp.close();


# Figure out which files to download.
num_to_download = 0
for i in range(0, len(entries)):
    entry = entries[i];
    if entry['type'] == 'M':
        entry['download'] = True;
        entries[i-1]['download'] = True;
        entries[i-2]['download'] = True;
    elif entry['type'] == 'P' or entry['type'] == 'E':
        entry['download'] = True;

    if entry['download'] == True:
        num_to_download += 1;

# List files to download
print(f"{num_to_download} files to download");
for i in range(0, len(entries)):
    entry = entries[i];
    if entry['download'] == False: continue;
    print(f"   {entry['filename']}");

# Download files
for i in range(0, len(entries)):
    entry = entries[i];
    if entry['download'] == False: continue;

    cmd = f"wget -c http://{IP}{entry['n']}";
    print(entry);
    print(cmd);
    ret = os.system(cmd);
    print(f"Return: {ret}");

    if ret == 0:
        # Download successful, remember it was downloaded
        new_files_done.append(entry['filename']);

# Write out files that were/are completely downloaded now
fp = open('.last', 'wt');
for f in new_files_done:
    fp.write(f + '\n');
fp.close();


